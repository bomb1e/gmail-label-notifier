// Client ID and API key from the Developer Console
const CLIENT_ID = '286747590009-5kun49uq5qurf2dhdpfvb99r4kac1sof.apps.googleusercontent.com';

const API_KEY = 'AIzaSyCiDd3onA8aaLV_B2yxeE4RbJi7xiIj-aM';

const CLIENT_SECRET = 'iVPKeOSFjHuCIg5n_m9BUHVG';

// Array of API discovery doc URLs for APIs used by the quickstart
const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
const SCOPES = 'https://mail.google.com';

const REDIRECT_URL = 'http://localhost';

const authorizeButton = document.getElementById('authorize-button');
const signoutButton = document.getElementById('signout-button');

const google = require('googleapis');
const OAuth2 = google.auth.OAuth2;

const md5 = require('md5');

const { remote, ipcRenderer, Menu, MenuItem } = require('electron')
const { BrowserWindow } = remote;

var authWindow = new BrowserWindow({ width: 800, height: 600, show: false, 'node-integration': false });
var oauth2Client = new OAuth2(
    CLIENT_ID,
    CLIENT_SECRET,
    REDIRECT_URL
);
window.gmail = google.gmail({
    version: 'v1',
    auth: oauth2Client
});
/**
 * IIFE for node compatibility
 */

(() => {
    // Check if there's a previously stored token
    if (window.localStorage.getItem('token')) {
        oauth2Client.credentials = JSON.parse(window.localStorage.getItem('token'));
        initApp();
    } else {
        getNewToken(oauth2Client, initApp);
    }

    authWindow.webContents.on('will-navigate', function(event, url) {
        handleCallback(url);
    });

    authWindow.webContents.on('did-get-redirect-request', function(event, oldUrl, newUrl) {
        handleCallback(newUrl);
    });

    // Reset the authWindow on close
    authWindow.on('close', function() {
        authWindow = null;
    }, false);

})();

function reInitializeAuthWindow() {
    authWindow = new BrowserWindow({ width: 800, height: 600, show: false, 'node-integration': false });
    authWindow.webContents.on('will-navigate', function(event, url) {
        handleCallback(url);
    });

    authWindow.webContents.on('did-get-redirect-request', function(event, oldUrl, newUrl) {
        handleCallback(newUrl);
    });

    // Reset the authWindow on close
    authWindow.on('close', function() {
        authWindow = null;
    }, false);
}

function getNewToken(oauth2Client, callback) {
    var authUrl = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES
    });
    authWindow.loadURL(authUrl);
    authWindow.show();
}


function handleCallback(url) {
    var raw_code = /code=([^&]*)/.exec(url) || null;
    var code = (raw_code && raw_code.length > 1) ? raw_code[1] : null;
    var error = /\?error=(.+)$/.exec(url);

    if (code || error) {
        // Close the browser if code found or error
        authWindow.destroy();
    }

    // If there is a code, proceed to get token from google
    if (code) {
        oauth2Client.getToken(code, function(err, token) {
            if (err) {
                console.log('Error while trying to retrieve access token', err);
                return;
            }
            oauth2Client.credentials = token;
            window.localStorage.setItem('token', JSON.stringify(token));
            initApp();
        });
    } else if (error) {
        alert('Oops! Something went wrong and we couldn\'t' + 'log you in using Google. Please try again.');
    }
}
let backgroundWindow;

function createBackgroundWindow() {
    backgroundWindow = new BrowserWindow({
        show: false
    });
    backgroundWindow.loadURL(`file://${__dirname}/background/background.html`);

    backgroundWindow.webContents.openDevTools();
}

function log(message, type) {
    switch (type) {
        case 'error':
            console.error(message);
        case 'info':
            console.log(message);
        case 'warning':
            console.warn(message);
        default:
            console.log(message);
    }
}


function initApp() {
    createBackgroundWindow();

    window.app = new Vue({
        el: '#app',
        data() {
            return {
                signedIn: false,
                labels: [],
                selectedLabel: {},
                emails: [],
                selectedEmail: {},
                currentView: 1,
                modalHidden: true,
                newWatcher: {
                    active: false,
                    time: 60,
                    labelA: '',
                    labelB: ''
                },
                watchers: [],
                selectedWatcher: {},
                labelAMessages: [],
                labelBMessages: [],
                notificationEmail: '',
                notificationEmailModalOpen: false,
                settingsOpen: false,
                deleteMessages: window.localStorage.getItem('deleteMessages') ? JSON.parse(window.localStorage.getItem('deleteMessages')) : false
            }
        },
        beforeMount() {
            this.updateSigninStatus(true);
            this.notificationEmail = window.localStorage.getItem('notificationEmail') ? JSON.parse(window.localStorage.getItem('notificationEmail')) : '';
        },
        mounted() {
            let vm = this
            document.getElementById('app').style.display = '';
            if (!(window.localStorage.getItem('notificationEmail'))) {
                this.notificationEmailModalOpen = true
            }

            setInterval(() => {
                if (window.localStorage.getItem('token')) {
                    gmail.users.labels.list({
                        'userId': 'me'
                    }, function(error, response) {

                        if (error) {
                            log(error, 'error');
                            return;
                        }
                        log('response', response);

                        vm.labels = response.labels.filter((label) => {
                            return label.type == 'user'
                        }).sort((a, b) => {
                            return a.name.toLowerCase() > b.name.toLowerCase()
                        });
                    });
                }
            }, 10000);
        },
        methods: {
            deleteChanged(e) {
                window.localStorage.setItem('deleteMessages', JSON.stringify(e.srcElement.checked))
            },
            saveNotificationEmail(e) {
                e.preventDefault();
                let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (this.notificationEmail == '' || !(re.test(this.notificationEmail))) {
                    alert('Please provide a valid email');
                    return;
                }
                window.localStorage.setItem('notificationEmail', JSON.stringify(this.notificationEmail));
                this.notificationEmailModalOpen = false
            },
            /**
             *  Called when the signed in status changes, to update the UI
             *  appropriately. After a sign-in, the API is called.
             */
            updateSigninStatus(isSignedIn) {
                let vm = this
                if (isSignedIn) {
                    this.signedIn = true
                    this.listLabels();
                    setInterval(() => {
                        vm.getWatchers();
                    }, 5000);
                } else {
                    this.signedIn = false
                }
            },
            /**
             *  Sign in the user upon button click.
             */
            handleAuthClick(event) {
                this.getNewToken(oauth2Client);
            },
            getNewToken(oauth2Client) {
                let vm = this;
                var authUrl = oauth2Client.generateAuthUrl({
                    access_type: 'offline',
                    scope: SCOPES
                });
                authWindow = new BrowserWindow({ width: 800, height: 600, show: false, 'node-integration': false });
                authWindow.webContents.on('will-navigate', function(event, url) {
                    vm.handleCallback(url, authWindow);
                });

                authWindow.webContents.on('did-get-redirect-request', function(event, oldUrl, newUrl) {
                    vm.handleCallback(newUrl, authWindow);
                });

                // Reset the authWindow on close
                authWindow.on('close', function() {
                    authWindow = null;
                }, false);
                authWindow.loadURL(authUrl);
                authWindow.show();
            },
            handleCallback(url, authWindow) {
                var raw_code = /code=([^&]*)/.exec(url) || null;
                var code = (raw_code && raw_code.length > 1) ? raw_code[1] : null;
                var error = /\?error=(.+)$/.exec(url);
                let vm = this
                if (code || error) {
                    // Close the browser if code found or error
                    authWindow.destroy();
                }

                // If there is a code, proceed to get token from google
                if (code) {
                    oauth2Client.getToken(code, function(err, token) {
                        if (err) {
                            console.log('Error while trying to retrieve access token', err);
                            return;
                        }
                        oauth2Client.credentials = token;
                        window.localStorage.setItem('token', JSON.stringify(token));
                        vm.signedIn = true
                        vm.listLabels();
                        setInterval(() => {
                            vm.getWatchers();
                        }, 5000);
                        if (!(window.localStorage.getItem('notificationEmail'))) {
                            vm.notificationEmailModalOpen = true
                        }
                    });
                } else if (error) {
                    alert('Oops! Something went wrong and we couldn\'t' + 'log you in using Google. Please try again.');
                }
            },

            /**
             *  Sign out the user upon button click.
             */
            handleSignoutClick() {
                this.signedIn = false;
                backgroundWindow = null;
                window.localStorage.clear();
                window.app = null
            },
            /**
             * Print all Labels in the authorized user's inbox. If no labels
             * are found an appropriate message is printed.
             */
            listLabels() {
                const vm = this
                gmail.users.labels.list({
                    'userId': 'me'
                }, function(error, response) {

                    if (error) {
                        log(error, 'error');
                        return;
                    }
                    log('response', response);

                    vm.labels = response.labels.filter((label) => {
                        return label.type == 'user';
                    });

                    window.localStorage.setItem('labels', JSON.stringify(vm.labels));
                    vm.getWatchers();
                    vm.selectLabel(vm.labels[0]);
                });
            },

            getWatchers() {
                this.watchers = window.localStorage.getItem('watchers') ? JSON.parse(window.localStorage.getItem('watchers')) : [];
            },

            /**
             * Opens messages when a label is clicked
             */

            selectLabel(label) {
                const vm = this;
                vm.emails = [];
                vm.selectedEmail = {};
                vm.selectedLabel = label;
                vm.listMessages(label.id)
                    .then((messages) => {
                        if (messages) {
                            messages.forEach((message) => {
                                vm.getMessage(message.id);
                            })
                            vm.selectedEmail = vm.emails[0];
                        }
                    })
                this.currentView = 1;
            },

            listMessages(labelId) {
                return new Promise((resolve, reject) => {
                    gmail.users.messages.list({
                        'userId': 'me',
                        'labelIds': [labelId],
                        'maxResults': 1
                    }, (error, response) => {
                        if (error) {
                            console.log('error', error)
                            return
                        }
                        resolve(response.messages)
                    })
                })
            },
            getMessage(messageId) {
                const vm = this
                gmail.users.messages.get({
                    'userId': 'me',
                    'id': messageId,
                    'format': 'full'
                }, (error, response) => {
                    if (error) {
                        console.log('error', error)
                    }
                    vm.emails.push(response)
                })
            },
            getHeader(headers, index) {
                let value;
                headers.forEach((header) => {
                    if (header.name == index) {
                        value = header.value;
                    }
                });
                return value
            },
            getBody(message) {
                var encodedBody = '';
                if (typeof message.parts === 'undefined') {
                    encodedBody = message.body.data;
                } else {
                    encodedBody = this.getHTMLPart(message.parts);
                }
                encodedBody = encodedBody.replace(/-/g, '+').replace(/_/g, '/').replace(/\s/g, '');
                return decodeURIComponent(escape(window.atob(encodedBody)));
            },
            getHTMLPart(arr) {
                for (var x = 0; x <= arr.length; x++) {
                    if (typeof arr[x].parts === 'undefined') {
                        if (arr[x].mimeType === 'text/html') {
                            return arr[x].body.data;
                        }
                    } else {
                        return this.getHTMLPart(arr[x].parts);
                    }
                }
                return '';
            },
            selectEmail(email) {
                this.selectedEmail = email;
            },
            isEmpty(obj) {
                for (var key in obj) {
                    if (obj.hasOwnProperty(key))
                        return false;
                }
                return true;
            },
            getSenderEmail(email) {
                return email.match(/\<(.*?)\>/) ? email.match(/\<(.*?)\>/)[1] : 'notfound@email.com';
            },
            getGravatarImage(email) {
                return `https://www.gravatar.com/avatar/${md5(this.getSenderEmail(email))}?d=mm`;
            },
            setWatcher(e) {
                e.preventDefault();
                this.newWatcher.id = `${this.newWatcher.labelA}-${this.newWatcher.labelB}`;
                this.watchers.push(this.newWatcher)
                window.localStorage.setItem('watchers', JSON.stringify(this.watchers))
                this.resetNewWatcher()
                this.modalHidden = true
            },
            resetNewWatcher() {
                this.newWatcher = {
                    active: false,
                    time: 60,
                    labelA: '',
                    labelB: ''
                }
            },
            selectWatcher(watcher) {
                if (watcher.labelA.length == 0 || watcher.labelB.length == 0) {
                    ipcRenderer.send('error', 'Please select some labels')
                }
                this.selectedWatcher = watcher
                window.localStorage.setItem('watchers', JSON.stringify(this.watchers.map((theWatcher) => {
                    return {
                        id: theWatcher.id,
                        active: theWatcher.id == watcher.id ? false : theWatcher.active,
                        labelA: theWatcher.labelA,
                        labelB: theWatcher.labelB,
                        time: theWatcher.time
                    }
                })))
                this.getWatchers()
                this.currentView = 2
            },
            deleteWatcher(watcherToDelete) {
                this.watchers = this.watchers.filter((watcher) => {
                    return watcher.id != watcherToDelete.id;
                })
                window.localStorage.setItem('watchers', JSON.stringify(this.watchers));
            },
            getLabelName(labelID) {
                return this.labels.find((label) => {
                    return label.id == labelID
                }).name
            }
        },
        computed: {
            sortedLabels() {
                return this.labels.sort((a, b) => {
                    return a.name.replace(/\//g, 'A').localeCompare(b.name.replace(/\//g, 'A'))
                })
            }
        }
    })

    Vue.component('message-content', {
        template: '#message-content',
        props: ['selectedEmail'],
        data() {
            return {

            }
        },
        methods: {
            getHeader(headers, index) {
                let value
                headers.forEach((header) => {
                    if (header.name == index) {
                        value = header.value;
                    }
                });
                return value
            },
            getBody(message) {
                var encodedBody = '';
                if (typeof message.parts === 'undefined') {
                    encodedBody = message.body.data;
                } else {
                    encodedBody = this.getHTMLPart(message.parts);
                }
                encodedBody = encodedBody.replace(/-/g, '+').replace(/_/g, '/').replace(/\s/g, '');
                return decodeURIComponent(escape(window.atob(encodedBody)));
            },
            getHTMLPart(arr) {
                for (var x = 0; x <= arr.length; x++) {
                    if (typeof arr[x].parts === 'undefined') {
                        if (arr[x].mimeType === 'text/html') {
                            return arr[x].body.data;
                        }
                    } else {
                        return this.getHTMLPart(arr[x].parts);
                    }
                }
                return '';
            }
        }
    })
    Vue.component('label-watcher-messages', {
        template: '#label-watcher-messages',
        props: ['labelId'],
        data() {
            return {
                messages: []
            }
        },
        beforeMount() {
            let vm = this
            vm.getLabelMessagesList(vm.labelId)
            setInterval(() => {
                vm.getLabelMessagesList(vm.labelId)
            }, 10000)
        },
        watch: {
            labelId() {
                this.getLabelMessagesList(this.labelId)
            }
        },
        methods: {
            getLabelMessagesList(labelId) {
                let vm = this
                let labelMessages = []
                gmail.users.messages
                    .list({
                        'userId': 'me',
                        'labelIds': [labelId],
                        'maxResults': 1
                    }, (error, response) => {
                        if (error) {
                            console.log(error);
                            return;
                        }
                        let messages = response.messages

                        if (!messages) {
                            return
                        }
                        messages.forEach((message) => {
                            gmail.users.messages.get({
                                'userId': 'me',
                                'id': message.id,
                                'format': 'full'
                            }, (error, response) => {
                                if (error) {
                                    console.log(error);
                                    return;
                                }
                                labelMessages.push(response)
                            });
                        });
                        vm.messages = labelMessages
                    })


            },
            getHeader(headers, index) {
                let value
                headers.forEach((header) => {
                    if (header.name == index) {
                        value = header.value;
                    }
                });
                return value
            },
            getSenderEmail(email) {
                return email.match(/\<(.*?)\>/) ? email.match(/\<(.*?)\>/)[1] : 'notfound@email.com';
            },
            getGravatarImage(email) {
                return `https://www.gravatar.com/avatar/${md5(this.getSenderEmail(email))}?d=mm`;
            },
        }
    })
}