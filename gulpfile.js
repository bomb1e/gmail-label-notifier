var gulp = require('gulp'),
    sass = require('gulp-sass'),
    csso = require('gulp-csso'),
    perroquet = require('gulp-perroquet'),
    rollup = require('gulp-rollup'),
    autoprefixer = require('gulp-autoprefixer'),
    clean = require('gulp-clean'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-minify')
htmlmin = require('gulp-html-minifier'),
    sourcemaps = require('gulp-sourcemaps'),
    seq = require('gulp-sequence'),
    babel = require('gulp-babel');

// Css
gulp.task('css', function(callback) {
    return gulp.src('dev/css/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('prod'))
});

gulp.task('css:compress', function() {
    return gulp.src('prod/css/**/*.css', { base: './' })
        .pipe(csso())
        .pipe(gulp.dest('.'));
});

// Js
gulp.task('js', function() {
    return gulp.src(['dev/js/**/*.js'])
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('prod'))
});
gulp.task('js:compress', function() {
    return gulp.src('prod/**/*.js', { base: './' })
        // .pipe(uglify())
        .pipe(gulp.dest('.'));
});


// Cleanup
gulp.task('clean', function() {
    return gulp.src(['prod/'], { read: false })
        .pipe(clean({ force: true }));
});

// Default build all
gulp.task('default', ['css', 'js']);

// Compressors
gulp.task('compress', ['css:compress', 'js:compress']);

// Release maker
gulp.task('release', seq('clean', 'default', 'compress'));

// Watcher
gulp.task('watch', ['default'], function() {
    gulp.watch('dev/**/*.scss', ['css']);
    gulp.watch(['dev/**/*.js'], ['js']);
});