// Client ID and API key from the Developer Console
const CLIENT_ID = '286747590009-5kun49uq5qurf2dhdpfvb99r4kac1sof.apps.googleusercontent.com';

const API_KEY = 'AIzaSyCiDd3onA8aaLV_B2yxeE4RbJi7xiIj-aM';

const CLIENT_SECRET = 'iVPKeOSFjHuCIg5n_m9BUHVG';

// Array of API discovery doc URLs for APIs used by the quickstart
const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
const SCOPES = 'https://www.googleapis.com/auth/gmail.labels https://www.googleapis.com/auth/gmail.metadata https://www.googleapis.com/auth/gmail.readonly https://mail.google.com/';

const REDIRECT_URL = 'http://localhost';

const google = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const { ipcRenderer } = require('electron');

let watchers = []

var oauth2Client = new OAuth2(
    CLIENT_ID,
    CLIENT_SECRET,
    REDIRECT_URL
);

(() => {
    window.gmail = google.gmail({
        version: 'v1',
        auth: oauth2Client
    });

    watchers = window.localStorage.getItem('watchers') ? JSON.parse(window.localStorage.getItem('watchers')) : []

    if (window.localStorage.getItem('token')) {
        oauth2Client.credentials = JSON.parse(window.localStorage.getItem('token'));
        setInterval(() => {
            watchLabels();
        }, 3000);
    } else {
        ipcRenderer.on('logged-in', (response) => {
            console.log('logged in!');
            oauth2Client.credentials = JSON.parse(window.localStorage.getItem('token'));
            setInterval(() => {
                watchLabels();
            }, 10000);
        })
    }


    let cleanOldMails = () => {
        // disabled / retry later
        if (localStorage.deleteMessages != "true") {
            cleanOldMails.restart();
            return;
        }

        // get the watched labels IDs
        let watchers = JSON.parse(localStorage.watchers),
            labels = JSON.parse(localStorage.labels),
            labels_to_clean = [];

        watchers.forEach((w) => {
            labels_to_clean.push(w.labelA);
            labels_to_clean.push(w.labelB);
        });

        // get the data associated (format: { id, name, type })
        labels_to_clean = labels.reduce(function(t, label) {
            if (labels_to_clean.indexOf(label.id) > -1) t.push(label);
            return t;
        }, []);

        // first we get all the messages
        cleanOldMails.todo = [];
        labels_to_clean.forEach((label) => {

            gmail.users.messages.list({
                userId: 'me',
                labelIds: [label.id],
                maxResults: 100

            }, (error, response) => {
                if (error) console.error(error);
                else {
                    let to_delete = response.messages.map((m) => m.id);
                    to_delete.shift(); // keep the first one
                    cleanOldMails.todo = cleanOldMails.todo.concat(to_delete);
                }

                // get message IDs
                labels_to_clean.splice(labels_to_clean.indexOf(label), 1);
                if (!labels_to_clean.length) {
                    // remove duplicates
                    cleanOldMails.todo = Array.from(new Set(cleanOldMails.todo));
                    cleanOldMails.clean();
                }
            });

        });
    };
    cleanOldMails.clean = () => {
        // end of messages to delete
        if (!cleanOldMails.todo.length) {
            cleanOldMails.restart();
            return;
        }

        // one message to delete
        let to_delete = cleanOldMails.todo.shift();
        console.log('deleting e-mail', to_delete);
        gmail.users.messages.delete({
            userId: 'me',
            id: to_delete
        }, (error, response) => {
            if (error) return console.error(error);
            console.log(response);
        });

        // next message in .25 seconds
        setTimeout(cleanOldMails.clean, 500);
    };
    cleanOldMails.restart = () => {
        if (cleanOldMails.timeout) clearTimeout(cleanOldMails.timeout);

        // retry in 2 minutes
        cleanOldMails.timeout = setTimeout(cleanOldMails, 2 * 60000);
    };
    cleanOldMails();


    /*
    setInterval(() => {
        console.log('delete messages interval')
        if (window.localStorage.getItem('labels') && window.localStorage.getItem('deleteMessages')) {
            console.log('delete messages')
            let labelsForDeletion;
            labelsForDeletion = JSON.parse(window.localStorage.getItem('labels'));
            labelsForDeletion.forEach((label) => {
                let messagesToShift;
                listMessages(label[5].id)
                    .then((response) => {
                        messagesToShift = response.messages.slice();
                        let messageIdsToDelete = [];
                        messagesToShift.shift()
                        messagesToShift.forEach((message) => {
                            messageIdsToDelete.push(message.id)
                        });
                        if (messageIdsToDelete.length > 0) {
                            gmail.users.messages.batchDelete({
                                'userId': 'me',
                                'ids': messageIdsToDelete
                            }, (error, response) => {
                                if (error) {
                                    console.log(error);
                                } else {
                                    console.log(response);
                                }
                            });
                        }
                    })
                    .catch((error) => {
                        console.log(error);
                    });

            });
        }
    }, 60000);*/
})();

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function listMessages(labelId) {
    return new Promise((resolve, reject) => {
        gmail.users.messages.list({
            'userId': 'me',
            'labelIds': [labelId],
            'maxResults': 100000
        }, (error, response) => {
            if (error) {
                reject(error);
                return;
            }
            resolve(response);
        });
    });
}

function getMessage(messageId, emails) {
    return new Promise((resolve, reject) => {
        gmail.users.messages.get({
            'userId': 'me',
            'id': messageId,
            'format': 'metadata'
        }, (error, response) => {
            if (error) {
                reject(error);
                return;
            }
            resolve(response);
        });
    });
}

function sendMessage(headers_obj, message) {


    var email = '';

    for (var header in headers_obj)
        email += header += ": " + headers_obj[header] + "\r\n";

    email += "\r\n" + message;

    gmail.users.messages.send({
        'userId': 'me',
        'resource': {
            'raw': window.btoa(email).replace(/\+/g, '-').replace(/\//g, '_')
        }
    }, (error, response) => {
        if (error) {
            console.log(error)
            return
        }
        console.log('email sent!', response);
    });
}

let labelMessages = {}
let lastMessageSent = '';

function watchLabels() {
    watchers = window.localStorage.getItem('watchers') ? JSON.parse(window.localStorage.getItem('watchers')) : []
    let lastLabelMessages = window.localStorage.getItem('lastLabelMessages') ? JSON.parse(window.localStorage.getItem('lastLabelMessages')) : {}
    if (watchers.length > 0) {
        watchers.forEach((watcher) => {
            lastLabelMessages = window.localStorage.getItem('lastLabelMessages') ? JSON.parse(window.localStorage.getItem('lastLabelMessages')) : {}
            if (!labelMessages[watcher.id]) {
                labelMessages[watcher.id] = {
                    labelA: '',
                    labelB: ''
                }
            }
            if (!lastLabelMessages[watcher.id]) {
                lastLabelMessages[watcher.id] = {
                    labelA: '',
                    labelB: ''
                }
            }

            // For label A
            listMessages(watcher.labelA)
                .then((response) => {
                    getMessage(response.messages[0].id)
                        .then((response) => {
                            labelMessages[watcher.id].labelA = {
                                time: response.internalDate / 1000,
                                messageId: response.id
                            }

                            // For label B
                            listMessages(watcher.labelB)
                                .then((response) => {
                                    getMessage(response.messages[0].id)
                                        .then((response) => {
                                            labelMessages[watcher.id].labelB = {
                                                time: response.internalDate / 1000,
                                                messageId: response.id
                                            }

                                            if ((labelMessages[watcher.id].labelA.messageId != lastLabelMessages[watcher.id].labelA.messageId || labelMessages[watcher.id].labelB.messageId != lastLabelMessages[watcher.id].labelB.messageId) && lastMessageSent != (labelMessages[watcher.id].labelA.messageId + '-' + labelMessages[watcher.id].labelB.messageId)) {

                                                let timeBetweenLabelMessages = labelMessages[watcher.id].labelA.messageId != lastLabelMessages[watcher.id].labelA.messageId ? labelMessages[watcher.id].labelA.time - lastLabelMessages[watcher.id].labelA.time : labelMessages[watcher.id].labelB.time - lastLabelMessages[watcher.id].labelB.time
                                                let timeBetweenMessagesInLabels = Math.abs(labelMessages[watcher.id].labelA.time - labelMessages[watcher.id].labelB.time)

                                                if (timeBetweenLabelMessages <= Number(watcher.time) || timeBetweenMessagesInLabels <= Number(watcher.time)) {
                                                    // Check has passed 🎉💥😎
                                                    // Send email and notify user on desktop

                                                    let labels = window.localStorage.getItem('labels') ? JSON.parse(window.localStorage.getItem('labels')) : []

                                                    let labelAName = labels.find((label) => {
                                                        return label.id == watcher.labelA
                                                    }).name;
                                                    let labelBName = labels.find((label) => {
                                                        return label.id == watcher.labelB
                                                    }).name;

                                                    // Construct and send email
                                                    let message = `A watcher triggered a notification!\n\nWhile watching for messages in the selected labels, [${labelMessages[watcher.id].labelA.messageId != lastLabelMessages[watcher.id].labelA.messageId ? labelAName : labelBName }] got a new message within ${timeBetweenLabelMessages < timeBetweenMessagesInLabels ? timeBetweenLabelMessages : timeBetweenMessagesInLabels} seconds which is less than ${watcher.time} seconds that was set`;
                                                    let to = window.localStorage.getItem('notificationEmail') ? JSON.parse(window.localStorage.getItem('notificationEmail')) : 'ericgithinjimaina@gmail.com'
                                                    let subject = `Watcher [${ labelAName + ' - ' + labelBName }] has triggered a notification`

                                                    // Check whether there's a message sent before within watcher timing and the new messages are from the same label
                                                    let lastMessageLabelAId = lastMessageSent.split('-')[0];
                                                    let lastMessageLabelBId = lastMessageSent.split('-')[1];

                                                    let currentMessageLabelAId = labelMessages[watcher.id].labelA.messageId;
                                                    let currentMessageLabelBId = labelMessages[watcher.id].labelB.messageId;


                                                    if (!(
                                                            lastMessageLabelAId == currentMessageLabelAId && lastMessageLabelBId != currentMessageLabelBId || // current labelA message is equal to the previous one and in labelB it has changed
                                                            lastMessageLabelBId == currentMessageLabelBId && lastMessageLabelAId != currentMessageLabelAId // current labelA message is equal to the previous one and in labelA it has changed
                                                        )) {
                                                        sendMessage({
                                                            'To': to,
                                                            'Subject': subject
                                                        }, message)
                                                    }


                                                    lastMessageSent = labelMessages[watcher.id].labelA.messageId + '-' + labelMessages[watcher.id].labelB.messageId
                                                        // Modify localstorage :D and send notification to main window
                                                    window.localStorage.setItem('watchers', JSON.stringify(watchers.map((theWatcher) => {
                                                        return {
                                                            id: theWatcher.id,
                                                            active: theWatcher.id == watcher.id ? true : theWatcher.active,
                                                            labelA: theWatcher.labelA,
                                                            labelB: theWatcher.labelB,
                                                            time: theWatcher.time
                                                        }
                                                    })));
                                                }
                                            }
                                            lastLabelMessages = labelMessages
                                            window.localStorage.setItem('lastLabelMessages', JSON.stringify(lastLabelMessages))
                                        })
                                        .catch((error) => {
                                            console.log(error)
                                        })
                                })
                        })
                        .catch((error) => {
                            console.log(error)
                        })
                })
                .catch((error) => {
                    console.log(error)
                })
        })

    }
}